/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';    
// import App from './src/components/Login/Splash';
// import App from './src/components/Login/Login';
// import App from './src/components/Login/Signup';
// import App from './src/components/Login/Report';
// import App from './src/components/advertiser/My_ads';

import { name as appName } from './app.json';

import { gestureHandlerRootHOC } from 'react-native-gesture-handler'
AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(App));
