import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import Routes from './src/navigation/routes';
import Routeskey from './src/navigation/routeskey';
const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor="black" barStyle="light-content" />

  
        <Routes />
    
      {/* <Text>ssss</Text> */}
    </View>
  );
};

export default App;
