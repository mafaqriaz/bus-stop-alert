import React, {Component} from 'react';
import Button from '../../utils/Button';
import firebase from '../../config/firebase';
import LoginStyle from './LoginStyle';
import Global from '../../utils/global';
import Constant from '../../utils/constant';

import {
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
  Platform,
  StyleSheet,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Alert from '../../utils/Alert';
import RoutesKey from '../../navigation/routeskey';

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: '',
      confirmpass: '',
      phone: '+923462337076',
      showModel: false,
      loading: false,
    };
  }
  componentDidMount() {}
  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  async imageUpload() {
    this.setState({isLoading: true});
    let result = await ImagePicker.showImagePicker({
      allowsEditing: true,
      aspect: [4, 6],
      quality: 0.3,
    });
    // let result = await ImagePicker.launchImageLibraryAsync();
    console.log('result', result);
    !result.cancelled
      ? this.uploadImageAsync(result.uri, 'test')
          .then(() => {
            this.setState({
              Alertbox: true,
              isLoading: false,
              ehead: 'Uploaded',
              error: 'Your Image is Uploaded Sucessfully',
            });
            // Alert.alert("Uploaded", "Your Image is Uploaded Sucessfully");
          })
          .catch(error => {
            // Alert.alert("Error", error.messsage);
            console.log('sssssssss', error);
          })
      : this.setState({
          Alertbox: true,
          isLoading: false,
          ehead: 'Error',
          error: 'Please Select Image',
        });
  }
  async uploadImageAsync(uri) {
    const blob = await new Promise((resolve, reject) => {
      console.log('a');
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        resolve(xhr.response);
        console.log('b');
      };
      xhr.onerror = function(e) {
        console.log(e);
        console.log('c');
        reject(new TypeError('Network request failed'));
      };
      console.log('d');
      xhr.responseType = 'blob';
      console.log('e');
      xhr.open('GET', uri, true);
      console.log('f');
      xhr.send(null);
    });
    console.log('blob', blob);
    const sessionId = new Date().getTime();
    const ref = firebase
      .storage()
      .ref('UserWorkImages')
      .child(`${sessionId}`);
    console.log('ref bloob', ref);
    const snapshot = await ref.put(blob);
    blob.close();

    const dURL = await snapshot.ref.getDownloadURL();
    this.setState({URL: dURL, loading: false});
    // console.log("dataurl", dURL);
  }
  /**async imageUpload() {
    this.setState({loading: true});
    let result = await ImagePicker.launchCameraAsync({
      quality: 0.5,
      allowsEditing: true,
      aspect: [4, 3],
    });
    // let result = await ImagePicker.launchImageLibraryAsync();
    console.log('result', result);
    !result.cancelled &&
      this.uploadImageAsync(result.uri, 'test')
        .then(() => {
          Alert.alert('Uploaded', 'Your Image is Uploaded Sucessfully');
        })
        .catch(error => {
          // Alert.alert("Error", error.messsage);
          console.log('sssssssss', error);
        });
  }**/
  Register() {
    console.log('state', this.state);
    this.setState({loading: true});
    const {
      // location,
      // validemail,
      // validpass,
      name,
      email,
      password,
      confirmpass,
      phone,
    } = this.state;
    const checkEmail = this.validateEmail(this.state.email);
    // if(!checkEmail)
    // {
    //     this.setState({ showModel: true, msg: " " })

    // }
    console.log('check no', checkEmail);
    if (
      email == null ||
      password == null ||
      name == null ||
      confirmpass == null ||
      phone == null ||
      email.trim() == '' ||
      password.trim() == '' ||
      name.trim() == '' ||
      confirmpass.trim() == '' ||
      phone.trim() == ''

      // validemail == false ||
      // validpass == false
    ) {
      this.setState({showModel: true, msg: 'pleaas fill all field'});
      // if (email == null) {
      //     this.setState({ validemail: false });
      // } else {
      //     if (email == "") {
      //         this.setState({ validemail: false });
      //     }
      // }
      // if (password == null) {
      //     this.setState({ validpass: false });
      // } else {
      //     if (password == "") {
      //         this.setState({ validpass: false });
      //     }
      // }
    } else {
      if (password == confirmpass) {
        firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then(s => {
            var today = new Date();
            var date =
              today.getFullYear() +
              '-' +
              (today.getMonth() + 1) +
              '-' +
              today.getDate();
            var time =
              today.getHours() +
              ':' +
              today.getMinutes() +
              ':' +
              today.getSeconds();
            var dateTime = date + ' ' + time;
            const uid = firebase.auth().currentUser.uid;
            const data = {
              Name: name,
              Email: email.toLowerCase(),
              Phone: phone,
              UserId: uid,
              Roll: 'driver',
              IsActive: true,
              IsDeleted: false,
              CreatedDate: dateTime,
            };
            firebase
              .database()
              .ref('users/' + uid)
              .set(data)
              .then(() => {
                // Alert.alert("Sign up", "You are suessfully Sign In.");
                // this.props.navigation.navigate("Thankyou", data);
                this.setState({
                  showModel: true,
                  msg: 'Successfully Registered',
                });
                Global.saveDataInPhone(Constant.USER_DATA_KEY, data);
              });
          })
          .catch(error => {
            console.log('erro r', error);
            this.setState({
              showModel: true,
              msg: error.message,
            });
          });
      } else {
        this.setState({showModel: true, msg: 'Confrm Password does not match'});
      }
    }
  }

  // login() {
  //     let { email, password } = this.state;

  //     let chk = this.validateEmail(email);
  //     if ((email, password)) {
  //         if (chk) {
  //             this.setState({ loader: true, conditionFlage: true });
  //             this.props.loginUser({
  //                 email,
  //                 password,
  //             });
  //         } else {
  //             this.setState({ showModel: true, msg: 'Incorrect Email' });
  //         }
  //     } else {
  //         this.setState({ showModel: true, msg: 'Fill all field' });
  //     }
  // }
  func = () => {
    // alert('func');
    this.setState({showModel: true});
  };
  func1 = () => {
    alert('func1');
  };
  onChange(name, val) {
    this.setState({[name]: val});
  }
  render() {
    let {
      loader,
      secure,
      name,
      email,
      password,
      confirmpass,
      phone,
    } = this.state;
    // console.log('data jo a rha he render', this.props);
    return (
      <ImageBackground
        style={{flex: 1}}
        resizeMode="cover"
        // resizeMode="contain"
        source={require('../../assets/images/bg5.png')}>
        <ScrollView>
          <View style={LoginStyle.top}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={LoginStyle.back}
                source={require('../../assets/images/back.png')}
              />
            </TouchableOpacity>
            <Text
              style={[
                LoginStyle.color,
                {
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginRight: 40,
                  alignSelf: 'center',
                },
              ]}>
              Register
            </Text>
            <TouchableOpacity>
              {/*
                            <Image
                                style={{ width: 30, height: 25 }}
                                source={require('../../assets/images/menu.png')}
                            /> */}
            </TouchableOpacity>
          </View>
          <Image
            style={{height: 150, width: 150, alignSelf: 'center'}}
            source={require('../../assets/images/icon.png')}
          />
          <View style={{alignItems: 'center'}}>
            <View style={LoginStyle.TextInputView}>
              <TextInput
                underlineColorAndroid="transparent"
                style={LoginStyle.TextInputStyle}
                placeholder="Driver Name"
                placeholderTextColor="#b2b2b2"
                onChangeText={this.onChange.bind(this, 'name')}
                value={name}
              />
            </View>
            <View style={LoginStyle.TextInputView}>
              <TextInput
                underlineColorAndroid="transparent"
                style={LoginStyle.TextInputStyle}
                placeholder="Phone"
                placeholderTextColor="#b2b2b2"
                onChangeText={this.onChange.bind(this, 'phone')}
                value={phone}
              />
            </View>
            <View style={LoginStyle.TextInputView}>
              <TextInput
                underlineColorAndroid="transparent"
                style={LoginStyle.TextInputStyle}
                placeholder="Email"
                placeholderTextColor="#b2b2b2"
                onChangeText={this.onChange.bind(this, 'email')}
                value={email}
              />
            </View>
            <View style={LoginStyle.TextInputView}>
              <TextInput
                underlineColorAndroid="transparent"
                style={LoginStyle.TextInputStyle}
                placeholder="Password"
                placeholderTextColor="#b2b2b2"
                onChangeText={this.onChange.bind(this, 'password')}
                value={password}
              />
            </View>
            <View style={LoginStyle.TextInputView}>
              <TextInput
                underlineColorAndroid="transparent"
                style={LoginStyle.TextInputStyle}
                placeholder="Confirm Password"
                placeholderTextColor="#b2b2b2"
                onChangeText={this.onChange.bind(this, 'confirmpass')}
                value={confirmpass}
              />
            </View>
          </View>
          {this.state.loading ? (
            <ActivityIndicator size="large" color="white" />
          ) : (
            <Button bg={true} title="Add" function={() => this.Register()} />
          )}
        </ScrollView>
        {this.state.showModel && (
          <Alert
            close={() => {
              this.setState({showModel: false, loading: false});
              this.props.navigation.navigate(RoutesKey.SPLASH);
            }}
            buttonHeading={'OK'}
            msg={this.state.msg}
          />
        )}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  button: {
    flexDirection: 'column',
    flex: 10,
    paddingBottom: 10,
  },

  ButtonStyle: {
    backgroundColor: '#07adb9',
    width: '90%',
    alignItems: 'center',
    color: 'blue',
    borderRadius: 5,
    alignSelf: 'center',
    marginVertical: 10,
    justifyContent: 'center',
    paddingVertical: 15,
  },
});
