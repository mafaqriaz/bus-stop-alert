import React, {Component} from 'react';
import Button from '../../utils/Button';
import Routeskey from '../../navigation/routeskey';
import LoginStyle from './LoginStyle';
import {
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
  Platform,
  StyleSheet,
} from 'react-native';
import Alert from '../../utils/Alert';
import Global from '../../utils/global';
import Constant from '../../utils/constant';
import firebase from '../../config/firebase';
import RoutesKey from '../../navigation/routeskey';
export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      pass: '',
      showModel: false,
    };
  }
  componentDidMount() {}

  login() {
    // this.props.navigation.openDrawer()
    // let { email, password } = this.state;
    this.setState({showModel: true, msg: 'login funvctionality in working'});

    // let chk = this.validateEmail(email);
    // if ((email, password)) {
    //     if (chk) {
    //         this.setState({ loader: true, conditionFlage: true });
    //         this.props.loginUser({
    //             email,
    //             password,
    //         });
    //     } else {
    //         this.setState({ showModel: true, msg: 'Incorrect Email' });
    //     }
    // } else {
    //     this.setState({ showModel: true, msg: 'Fill all field' });
    // }
  }
  func = () => {
    alert('func');
  };
  func1 = () => {
    alert('func1');
  };
  onChange(name, val) {
    this.setState({[name]: val});
  }

  Loginfunction = () => {
    if (
      this.state.email == null ||
      this.state.pass == null ||
      this.state.email.trim() == '' ||
      this.state.pass.trim() == ''
    ) {
      if (this.state.email == null) {
        // this.setState({ validemail: false });
        this.setState({showModel: true, msg: 'please fill your email'});
      } else {
        if (this.state.email.trim() == '') {
          // this.setState({ validemail: false });
          this.setState({showModel: true, msg: 'please fill your email'});
        }
      }
      if (this.state.pass == null) {
        // this.setState({ validpass: false });
        this.setState({showModel: true, msg: 'please fill your password'});
      } else {
        if (this.state.pass.trim() == '') {
          // this.setState({ validpass: false });
          this.setState({showModel: true, msg: 'please fill your password'});
        }
      }
    } else {
      this.setState({loading: true}, () => {
        firebase
          .auth()
          .signInWithEmailAndPassword(this.state.email, this.state.pass)
          .then(res => {
            console.log('signInwithEmail wala response', res);
            firebase.auth().onAuthStateChanged(userid => {
              if (userid) {
                var ids = userid.uid;
                firebase
                  .database()
                  .ref('users')
                  .child(ids)
                  .once('value')
                  .then(snapShot => {
                    // console.log(snapShot.val())
                    // console.log(snapShot.val().roll)

                    // const dataInString = JSON.stringify(snapShot.val());
                    Global.saveDataInPhone(
                      Constant.USER_DATA_KEY,
                      snapShot.val(),
                    );
                    this.setState({showModel: true, msg: 'Login Successfully'});
                    // this.props.navigation.navigate(Routeskey.SPLASH)
                    // console.log(snapShot.val().roll)

                    // var role = snapShot.val().Roll;
                    // if (role == 1) {
                    //   this.setState({ loading: false }, () => {
                    //     // this.props.navigation.navigate("freelancerStack");
                    //   }); //Freelancer
                    // }
                    // else if (role == 2) {
                    //   //user
                    //   this.setState({ loading: false }, () => {
                    //     // this.props.navigation.navigate("userStack");
                    //   }); //Freelancer
                    // }
                    // else if (role == 3) {//agent
                    // //   this.props.navigation.navigate("agentStack")
                    // }
                    // else if (role == 4) {
                    //   console.log("vendor tack")
                    // //   this.props.navigation.navigate("vendortStack")
                    // }
                  });
              } else {
                // console.log("ffffffffff");
              }
            });
          })
          .catch(error => {
            console.log('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM', error);
            // this.setState({ loading: false, alertbox: true })
            if (error && error.message) {
              this.setState({showModel: true, msg: error.message});
            } else {
              this.setState({
                showModel: true,
                msg: 'The email address is badly formatted.',
              });
            }
          });
      });
    }
  };

  render() {
    let {email, pass} = this.state;
    // console.log('data jo a rha he render', this.props);

    console.log('login', email);
    console.log('PAssswor', pass);

    return (
      <ImageBackground
        style={{flex: 1, alignItems: 'center'}}
        resizeMode="cover"
        // resizeMode="contain"
        source={require('../../assets/images/bg2.png')}>
        <View style={LoginStyle.share}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Image
              style={LoginStyle.back}
              source={require('../../assets/images/back.png')}
            />
          </TouchableOpacity>
          <Text
            style={[
              LoginStyle.color,
              {fontSize: 20, fontWeight: 'bold', alignSelf: 'center'},
            ]}>
            Login
          </Text>
        </View>
        <Image
          style={LoginStyle.Logoimage}
          source={require('../../assets/images/icon.png')}
        />
        <View style={LoginStyle.TextInputView}>
          <TextInput
            underlineColorAndroid="transparent"
            style={LoginStyle.TextInputStyle}
            placeholder="Email"
            placeholderTextColor="#b2b2b2"
            onChangeText={this.onChange.bind(this, 'email')}
            value={email}
          />
        </View>
        <View style={LoginStyle.TextInputView}>
          <TextInput
            underlineColorAndroid="transparent"
            secureTextEntry={true}
            style={LoginStyle.TextInputStyle}
            placeholder="Password"
            placeholderTextColor="#b2b2b2"
            onChangeText={this.onChange.bind(this, 'pass')}
            value={pass}
          />
        </View>
        {/* <Button bg={true} title="Login" function={() => this.Loginfunction()} /> */}

        {this.state.loading ? (
          <ActivityIndicator size="large" color="white" />
        ) : (
          <Button
            bg={true}
            title="Login"
            function={() => {
              this.Loginfunction();
              // this.setState({ showModel: true, msg: "login funvctionality in working" })
            }}
          />
        )}

        <View style={LoginStyle.forget}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(Routeskey.FORGET_PASSWORD);
            }}>
            <Text style={{color: '#fff', fontSize: 12}}>Forget password ?</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(Routeskey.SIGN_UP);
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{color: '#fff', fontSize: 12}}>
                Don't have account ?{' '}
              </Text>
              <Text style={{color: '#46639d', fontSize: 12}}>Signup</Text>
            </View>
          </TouchableOpacity>
        </View>
        {this.state.showModel && (
          <Alert
            close={() => {
              this.setState({showModel: false, loading: false});
              this.props.navigation.navigate(RoutesKey.SPLASH);
            }}
            buttonHeading={'OK'}
            msg={this.state.msg}
          />
        )}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  button: {
    flexDirection: 'column',
    flex: 10,
    paddingBottom: 10,
  },

  ButtonStyle: {
    backgroundColor: '#07adb9',
    width: '90%',
    alignItems: 'center',
    color: 'blue',
    borderRadius: 5,
    alignSelf: 'center',
    marginVertical: 10,
    justifyContent: 'center',
    paddingVertical: 15,
  },
});
