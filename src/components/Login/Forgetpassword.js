import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from './LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

export default class Forget extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
        };
    }
    componentDidMount() { }

    login() {
        let { email, password } = this.state;

        let chk = this.validateEmail(email);
        if ((email, password)) {
            if (chk) {
                this.setState({ loader: true, conditionFlage: true });
                this.props.loginUser({
                    email,
                    password,
                });
            } else {
                this.setState({ showModel: true, msg: 'Incorrect Email' });
            }
        } else {
            this.setState({ showModel: true, msg: 'Fill all field' });
        }
    }
    func = () => {
        alert('func');
    };
    func1 = () => {
        alert('func1');
    };
    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;
        console.log('data jo a rha he render', this.props);
        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg5.png')}>
                <ScrollView>
                    <View style={LoginStyle.share}>

                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>


                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()}> */}
                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",
                                alignSelf: "center"
                            }]}>Forget Password</Text>
                        {/* </TouchableOpacity> */}
                    </View>
                    <Image
                        style={LoginStyle.Logo}
                        source={require('../../assets/images/KEY.png')}
                    />
                    <View style={{ alignItems: "center" }}>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"
                                style={LoginStyle.TextInputStyle}
                                placeholder="Password"
                                placeholderTextColor="#b2b2b2"
                                onChangeText={this.onChange.bind(this, 'password')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"
                                style={LoginStyle.TextInputStyle}
                                placeholder="Forget Password"
                                placeholderTextColor="#b2b2b2"
                                onChangeText={this.onChange.bind(this, 'fpassword')}
                            // value={email}
                            />
                        </View>
                    </View>
                    <Button bg={true} title="Send" function={this.func} />


                </ScrollView>
            </ImageBackground >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    button: {
        flexDirection: 'column',
        flex: 10,
        paddingBottom: 10,
    },

    ButtonStyle: {
        backgroundColor: '#07adb9',
        width: '90%',
        alignItems: 'center',
        color: 'blue',
        borderRadius: 5,
        alignSelf: 'center',
        marginVertical: 10,
        justifyContent: 'center',
        paddingVertical: 15,
    },
});


