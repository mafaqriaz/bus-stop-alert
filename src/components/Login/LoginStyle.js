const LoginStyle = {
  share1: {
    backgroundColor: '#46639d',
    width: 70,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 25,
    marginTop: 30,
    elevation: 7,
  },
  color: {
    color: '#ffffff',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  Logoimage: {
    width: 300,
    height: 300,
  },
};

export default LoginStyle;
