import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView, DatePickerAndroid, DatePickerIOS,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class AdvertiserDetail extends Component {
    constructor(props) {
        super(props);
        this.setDate = this.setDate.bind(this);
        this.state = {
            dateios: false,
            chosenDate: new Date(),
            chosenAndroidTime: '00:00',
            androidDate: `${new Date().getUTCDate()}/${new Date().getUTCMonth() + 1}/${new Date().getUTCFullYear()}`,
            value: 50,
        };
    }
    componentDidMount() { }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }

    setDateAndroid = async () => {
        try {
            const {
                action, year, month, day,
            } = await DatePickerAndroid.open({
                date: new Date(),
                minDate: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({ androidDate: `${day}/${month + 1}/${year}` });
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    };


    onChange(name, val) {
        this.setState({ [name]: val });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg3.png')}>
                <ScrollView>
                    <View style={LoginStyle.top}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>
                        <Text style={[LoginStyle.color, {
                            fontSize: 18, fontWeight: "bold",
                            alignSelf: "center"
                        }]}>Advertiser</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                            <Image
                                style={{ width: 25, height: 25, resizeMode: "contain" }}
                                source={require('../../assets/images/menu.png')}
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ paddingHorizontal: 10 }} >

                        <View style={{ alignSelf: "center", flexDirection: "row", alignItems: "center" }}>

                            <Text style={{
                                color: "#fff", fontSize: 28, fontWeight: "bold"
                            }}>PBS
                            </Text>
                            <Image
                                style={{ width: 20, height: 25, resizeMode: "contain" }}
                                source={require('../../assets/images/profile.png')}
                            />
                        </View>
                        <Text style={{
                            color: "#fff", fontSize: 18,
                        }}>Area
                        </Text>


                        <View style={{
                            flex: 1, marginVertical: 10, borderRadius: 5, alignItems: "center",
                            paddingVertical: 5, backgroundColor: "#fff", flexDirection: "row", height: 150
                        }}>


                        </View>
                        <Text style={{
                            color: "#fff", fontSize: 18,
                        }}>Add
                        </Text>
                        <View style={{ alignSelf: "center", flexDirection: "row", alignItems: "center" }}>

                            <Text style={{
                                color: "#fff", fontSize: 22, fontWeight: "bold"
                            }}>Package : $450/month
</Text>

                        </View>
                    </View>

                </ScrollView>
                <Button bg={true} title="Pause Ads" function={() => { }} />
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



