import React, { Component } from 'react';
import Button from '../../utils/Button';
import { BarChart, Grid } from 'react-native-svg-charts'

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class Dashboard extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;
        const fill = 'rgb(134, 65, 244)'
        const data = [50, 10, 40, 95, null, 85, undefined, 0, 35, 53, 24, 50,]
        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top}>
                        <TouchableOpacity >

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/busb.png')}
                            />
                        </TouchableOpacity>
                        <Text style={[LoginStyle.color, {
                            fontSize: 18, fontWeight: "bold",

                            alignSelf: "center"
                        }]}>Dashboard</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                            <Image
                                style={{ width: 25, height: 25 }}
                                source={require('../../assets/images/menu.png')}
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ flexDirection: "row" }} >
                        <View style={LoginStyle.circle}>
                            <Image
                                style={{ width: 120, height: 120 }}
                                source={require('../../assets/images/bachi.png')}
                            />
                        </View>
                        <View style={{ alignSelf: "center", justifyContent: "center" }}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>User Count</Text>
                            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 42 }}>392</Text>
                        </View>

                    </View>
                    <View style={{ marginTop: 20, flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20 }} >
                        <Text style={{ color: "#fff", fontSize: 18 }}>User Count</Text>
                        <View style={{ paddingHorizontal: 2, backgroundColor: "#fff", borderRadius: 5 }}>

                            <Image
                                style={{ width: 30, height: 20 }}
                                source={require('../../assets/images/download.png')}
                            />
                        </View>

                    </View>
                    <View style={{ width: "90%", alignSelf: "center", marginVertical: 10, backgroundColor: "#fff", padding: 10, borderRadius: 10 }}>
                        <BarChart
                            style={{ height: 150 }}
                            data={data}
                            svg={{ fill }}
                        >

                        </BarChart>
                    </View>

                </ScrollView >
                {
                    this.state.showModel && (
                        <Alert
                            close={() => {
                                this.setState({ showModel: false });

                            }}
                            buttonHeading={"OK"}
                            msg={"SignUp Functionality in Working"}
                        />
                    )
                }
            </ImageBackground >
        );
    }
}



