import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput, Dimensions,
    KeyboardAvoidingView, DatePickerAndroid, DatePickerIOS,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Alert from "../../utils/Alert"
const options = {
    title: 'Select Avatar',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
import { BarChart, Grid } from 'react-native-svg-charts'
export default class Report extends Component {
    constructor(props) {
        super(props);
        this.setDate = this.setDate.bind(this);
        this.state = {
            dateios: false,
            chosenDate: new Date(),
            chosenAndroidTime: '00:00',
            androidDate: `${new Date().getUTCDate()}/${new Date().getUTCMonth() + 1}/${new Date().getUTCFullYear()}`,
            value: 50,
        };
    }
    componentDidMount() { }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }
    img = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
            }
        });
    }
    setDateAndroid = async () => {
        try {
            const {
                action, year, month, day,
            } = await DatePickerAndroid.open({
                date: new Date(),
                minDate: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({ androidDate: `${day}/${month + 1}/${year}` });
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    };


    onChange(name, val) {
        this.setState({ [name]: val });
    }
    render() {
        let { loader, secure } = this.state;
        const fill = 'rgb(134, 65, 244)'
        const data = [50, 10, 40, 95, null, 85, undefined, 0, 35, 53, 24, 50,]

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg3.png')}>
                <ScrollView>
                    <View style={LoginStyle.top}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>
                        <Text style={[LoginStyle.color, {
                            fontSize: 18, fontWeight: "bold",
                            alignSelf: "center"
                        }]}>Reports</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                            <Image
                                style={{ width: 25, height: 25, resizeMode: "contain" }}
                                source={require('../../assets/images/menu.png')}
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ paddingHorizontal: 10 }} >
                        <Text style={{
                            color: "#fff", fontSize: 18, fontWeight: "bold"
                        }}>
                            Financial Reports
                        </Text>

                        <View style={{
                            flex: 1, marginVertical: 10, borderRadius: 5, alignItems: "center",
                            paddingVertical: 5, backgroundColor: "#fff", flexDirection: "row"
                        }}>
                            <Text style={{ marginHorizontal: 20, flex: 1 }}> {this.state.androidDate}</Text>
                            <Text style={{ fontSize: 26 }}>|</Text>
                            <TouchableOpacity onPress={() => { Platform.OS === 'ios' ? this.setState({ dateios: true }) : this.setDateAndroid() }} style={{ marginHorizontal: 10 }}>

                                <Image
                                    style={{ width: 25, height: 25, resizeMode: "contain" }}
                                    source={require('../../assets/images/calendar.png')}
                                />
                            </TouchableOpacity>
                            {/* <TouchableOpacity onPress={() => { this.img() }} style={{ marginHorizontal: 10 }}>

                                <Text>adas</Text>
                            </TouchableOpacity> */}

                        </View>

                        {/* <DatePickerIOS date={new Date()} /> */}
                        {
                            this.state.dateios && Platform.OS === 'ios' ?
                                <View style={{ backgroundColor: "#fff" }}>
                                    <DatePickerIOS
                                        style={{ backgroundColor: "#fff", width: "90%", alignSelf: "center", justifyContent: 'center' }}

                                        date={this.state.chosenDate}
                                        onDateChange={this.setDate}
                                    />
                                    <TouchableOpacity style={{ width: "20%", alignItems: "center", justifyContent: "center", backgroundColor: "#b2b2b2", alignSelf: "center", marginVertical: 5 }} onPress={() => { this.setState({ dateios: false }) }}>
                                        <Text>Ok</Text>

                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                    </View>
                    <Button bg={true} title="Export" function={() => { }} />

                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}

            </ImageBackground >
        );
    }
}



