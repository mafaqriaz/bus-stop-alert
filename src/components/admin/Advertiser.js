import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"
import RoutesKey from '../../navigation/routeskey';

export default class Accounts extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",

                            }]}>Accounts</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 2,

                            alignSelf: "center"
                        }}>
                            <TouchableOpacity onPress={() => {

                            }} style={LoginStyle.export}>
                                <Text style={LoginStyle.color}>Export All</Text>
                            </TouchableOpacity>

                        </View>

                    </View>
                    {
                        [1, 2, 4].map(() => {
                            return (
                                <View style={LoginStyle.Accounts} >
                                    <View style={LoginStyle.circleSmall}>
                                        <Image
                                            style={{ width: 80, height: 80, resizeMode: "contain" }}
                                            source={require('../../assets/images/bachi.png')}
                                        />
                                    </View>
                                    <View style={{ flex: 1, alignSelf: "center", justifyContent: "center" }}>

                                        <Text style={{ color: "#fff", fontSize: 18, marginVertical: 2 }}>Driver Name</Text>
                                        <View style={{ alignSelf: "center", justifyContent: "center" }}>

                                            <Text style={{ color: "#fff", marginHorizontal: 5 }}>
                                                Lorem Ipsum is simply dummy text of the printing.
                        </Text>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: "space-around", paddingHorizontal: 5 }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate(RoutesKey.USER_DETAIL)}>

                                            <Image
                                                style={{ width: 23, height: 23, resizeMode: "contain" }}
                                                source={require('../../assets/images/edit.png')}
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity>

                                            <Image
                                                style={{ width: 23, height: 23, resizeMode: "contain" }}
                                                source={require('../../assets/images/small.png')}
                                            />
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            )
                        })
                    }






                </ScrollView>
                <View style={{ alignSelf: "center", alignItems: "center", padding: 2, paddingVertical: 5, marginVertical: 10, borderRadius: 5 }}>
                    <TouchableOpacity >
                        <View style={{
                            height: 24, width: 24, alignItems: "center",
                            justifyContent: "center", borderRadius: 24 / 2, borderColor: "#b2b2b2",
                            borderWidth: 0.5
                        }}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>+</Text>

                        </View>
                    </TouchableOpacity>
                    <Text style={{ color: "#fff" }}>Add new user</Text>
                </View>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



