import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class Help extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg3.png')}>
                <ScrollView>
                    <View style={LoginStyle.top}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>
                        <Text style={[LoginStyle.color, {
                            fontSize: 18, fontWeight: "bold",

                            alignSelf: "center"
                        }]}>Help</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                            <Image
                                style={{ width: 25, height: 25 }}
                                source={require('../../assets/images/menu.png')}
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ paddingHorizontal: 10 }} >
                        <Text style={{ color: "#fff", fontSize: 16, lineHeight: 30 }}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                             when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                              It has survived not only five centuries, but also the leap into electronic typesetting,
                              remaining essentially unchanged. It was popularised in the 1960s with the release of
                              Letraset sheets containing Lorem Ipsum passages,

                              and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


                     </Text>
                        <Text style={{ color: "#fff", fontSize: 16, lineHeight: 30 }}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                             when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                              It has survived not only five centuries, but also the leap into electronic typesetting,
                              remaining essentially unchanged. It was popularised in the 1960s with the release of
                              Letraset sheets containing Lorem Ipsum passages,

                              and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


                     </Text>

                    </View>

                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



