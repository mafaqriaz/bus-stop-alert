import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput, Switch,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"
import Notification from "../../utils/Notification"

import RoutesKey from '../../navigation/routeskey';

export default class My_ads extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false,
            audio: false,
            vibrate: false,
            light: false,
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure, audio, light, vibrate, setIsEnabled } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",

                            }]}>My Ads</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 10,

                            alignSelf: "center"
                        }}>
                            <TouchableOpacity onPress={() => this.setState({ showNotification: true })} >
                                {false ?
                                    <Image
                                        style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                        source={require('../../assets/images/notification.png')}
                                    /> :
                                    <Image
                                        style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                        source={require('../../assets/images/rednoti.png')}
                                    />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/images/menu.png')}
                                />
                            </TouchableOpacity>

                        </View>

                    </View>


                    <View >

                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>

                            <Text style={{ color: "#fff", fontSize: 18, marginVertical: 10, marginHorizontal: 20 }}>My Areas</Text>
                            <TouchableOpacity>

                                <Image
                                    style={{ width: 10, height: 10, marginVertical: 20, marginHorizontal: 20 }}
                                    source={require('../../assets/images/edit.png')}
                                />
                            </TouchableOpacity>
                        </View>

                    </View>

                    <View style={{ alignItems: "center" }}>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="Area 15"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'oldPass')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="Area 01"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'newPass')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="Area 88"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'comfirmPass')}
                            // value={email}
                            />
                        </View>
                    </View>


                    <View style={{ flexDirection: "column", justifyContent: "space-between" }}>

                        <Text style={{ color: "#fff", fontSize: 18, marginVertical: 10, marginHorizontal: 20 }}>My Areas</Text>
                        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate(RoutesKey.UPLOAD_YOUR_ADD)} >

                                <Image
                                    style={{ width: 100, height: 100, resizeMode: "contain", marginHorizontal: 20 }}
                                    source={require('../../assets/images/upload_icon.png')}
                                />

                            </TouchableOpacity>
                            <TouchableOpacity>

                                <Image
                                    style={{ width: 50, height: 50, margin: 25 }}
                                    source={require('../../assets/images/edit.png')}
                                />
                            </TouchableOpacity>
                        </View>



                        {this.state.loading ? <ActivityIndicator size="large" color="white" /> : <Button bg={true} title="Paypal/Payment method" function={() => { }} />}

                    </View>




                </ScrollView>
                {
                    this.state.showModel && (
                        <Alert
                            close={() => {
                                this.setState({ showModel: false });

                            }}
                            buttonHeading={"OK"}
                            msg={"SignUp Functionality in Working"}
                        />
                    )
                }
                {
                    this.state.showNotification && (
                        <Notification
                            close={() => {
                                this.setState({ showNotification: false });

                            }}
                        // buttonHeading={"OK"}
                        // msg={"SignUp Functionality in Working"}
                        />
                    )
                }
            </ImageBackground >
        );
    }
}



