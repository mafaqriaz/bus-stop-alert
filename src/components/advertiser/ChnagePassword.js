import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class ChanegePassword extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",

                            }]}>Change Password</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 10,

                            alignSelf: "center"
                        }}>

                            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/images/menu.png')}
                                />
                            </TouchableOpacity>

                        </View>

                    </View>


                    <View style={{ alignItems: "center" }}>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="Old Password"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'oldPass')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="New Password"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'newPass')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="Confirm Password"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'comfirmPass')}
                            // value={email}
                            />
                        </View>
                    </View>
                    {/* <View style={{ alignSelf: "center", backgroundColor: "#fff", padding: 2, paddingHorizontal: 15, borderRadius: 5 }}>
                        <TouchableOpacity>

                            <Image
                                style={{ width: 23, height: 23, resizeMode: "contain" }}
                                source={require('../../assets/images/small.png')}
                            />
                        </TouchableOpacity>
                    </View> */}
                    {this.state.loading ? <ActivityIndicator size="large" color="white" /> : <Button bg={true} title="Change Password" function={() => this.Loginfunction()} />}


                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



