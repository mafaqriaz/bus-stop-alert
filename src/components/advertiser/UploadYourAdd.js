import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class UploadYourAdd extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",
                            }]}>Upload Your add</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 10,

                            alignSelf: "center"
                        }}>
                            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                                <Image
                                    style={{ width: 23, height: 23 }}
                                    source={require('../../assets/images/menu.png')}
                                />
                            </TouchableOpacity>

                        </View>

                    </View>

                    <View style={{ paddingVertical: 50 }}>
                        <View style={{
                            width: 100, marginRight: 15, marginLeft: 15,
                            height: 100, alignSelf: "center",
                            justifyContent: "center", alignItems: "center"
                        }}>
                            <Image
                                style={{ width: 150, height: 80 }}
                                source={require('../../assets/images/upload_icon.png')}
                            />
                            <Text style={{ color: "white" }}>Select file</Text>
                        </View>


                    </View>
                    <View style={{ flex: 1, marginHorizontal: 25 }}>
                        <Text style={{ color: "#fff", textAlign: "left", marginVertical: 5, }}>
                            Add requirements
                        </Text>
                        <Text style={{ color: "#fff", textAlign: "left", marginVertical: 5 }}>
                            1. Your file must be in a MP3 format.
                        </Text>
                        <Text style={{ color: "#fff", textAlign: "left", marginVertical: 5 }}>
                            2. Your add may be no longer than 60 seconds.
                        </Text>
                    </View>

                    {this.state.loading ? <ActivityIndicator size="large" color="white" /> : <Button bg={true} title="Start my add" function={() => this.Loginfunction()} />}


                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



