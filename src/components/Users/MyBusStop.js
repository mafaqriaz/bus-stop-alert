import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';
import Notification from "../../utils/Notification"


import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"

export default class MyBusStop extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>

                <View style={LoginStyle.top1}>
                    <TouchableOpacity style={{
                        position: "absolute",
                        left: 10, top: 5
                    }} onPress={() => this.props.navigation.goBack()}>

                        <Image
                            style={LoginStyle.back}
                            source={require('../../assets/images/back.png')}
                        />
                    </TouchableOpacity>

                    <View style={{ alignSelf: "center" }}>

                        <Text style={[LoginStyle.color, {
                            fontSize: 18, fontWeight: "bold",

                        }]}>My Bus Stop</Text>
                    </View>
                    <View style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        position: "absolute",
                        right: 10,

                        alignSelf: "center"
                    }}>
                        <TouchableOpacity onPress={() => this.setState({ showNotification: true })} >
                            {false ?
                                <Image
                                    style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                    source={require('../../assets/images/notification.png')}
                                /> :
                                <Image
                                    style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                    source={require('../../assets/images/rednoti.png')}
                                />
                            }
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                            <Image
                                style={{ width: 20, height: 20 }}
                                source={require('../../assets/images/menu.png')}
                            />
                        </TouchableOpacity>

                    </View>

                </View>
                <View style={{ flex: 1 }}>
                    <ImageBackground
                        style={{ flex: 1 }}
                        resizeMode="cover"
                        // resizeMode="contain"
                        source={require('../../assets/images/bigmap.png')}>

                        {this.state.loading ? <ActivityIndicator size="large" color="white" /> :
                            <View style={{ position: "absolute", bottom: 10, width: "100%" }}>

                                <Button bg={true} title="Add New Bus Stop" function={() => { }} />
                            </View>
                        }
                    </ImageBackground>
                </View>







                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
                       {
                    this.state.showNotification && (
                        <Notification
                            close={() => {
                                this.setState({ showNotification: false });

                            }}
                        // buttonHeading={"OK"}
                        // msg={"SignUp Functionality in Working"}
                        />
                    )
                }
            </ImageBackground >
        );
    }
}



