import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"
import RoutesKey from '../../navigation/routeskey';


export default class Profile extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} >

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/busb.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",

                            }]}>Profile</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 10,

                            alignSelf: "center"
                        }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate(RoutesKey.EDIT_PROFILE)}>

                                <Image
                                    style={{ width: 20, height: 20, marginHorizontal: 20 }}
                                    source={require('../../assets/images/edit.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/images/menu.png')}
                                />
                            </TouchableOpacity>

                        </View>

                    </View>

                    <View style={{ paddingVertical: 40 }} >
                        <View style={LoginStyle.circleTop}>
                            <Image
                                style={{ width: 90, height: 90 }}
                                source={require('../../assets/images/bachi.png')}
                            />
                            <View style={{ height: 10, position: "absolute", width: 10, bottom: 0, right: 15 }}>
                                <TouchableOpacity onPress={() => { alert("image") }}>

                                    <Image
                                        style={{ width: 20, height: 20, resizeMode: "contain" }}
                                        source={require('../../assets/images/photo-camera.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>


                    </View>

                    <View style={{ alignItems: "center" }}>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="John doe"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'name')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="(123) 754 564 987"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'phone')}
                            // value={email}
                            />
                        </View>
                        <View style={LoginStyle.TextInputView}>
                            <TextInput
                                underlineColorAndroid="transparent"

                                style={LoginStyle.TextInputStyle}
                                placeholder="david@gmail.com"
                                placeholderTextColor="#fff"
                                onChangeText={this.onChange.bind(this, 'email')}
                            // value={email}
                            />
                        </View>
                    </View>
                    {/* <View style={{ alignSelf: "center", backgroundColor: "#fff", padding: 2, paddingHorizontal: 15, borderRadius: 5 }}>
                        <TouchableOpacity>

                            <Image
                                style={{ width: 23, height: 23, resizeMode: "contain" }}
                                source={require('../../assets/images/small.png')}
                            />
                        </TouchableOpacity>
                    </View> */}

                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



