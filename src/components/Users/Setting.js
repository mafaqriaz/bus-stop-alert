import React, { Component } from 'react';
import Button from '../../utils/Button';

import LoginStyle from '../Login/LoginStyle';

import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput, Switch,
    KeyboardAvoidingView,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet,
} from 'react-native';

import Alert from "../../utils/Alert"
import Notification from "../../utils/Notification"

import RoutesKey from '../../navigation/routeskey';

export default class MyBusStop extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            showModel: false,
            audio: false,
            vibrate: false,
            light: false,
        };
    }
    componentDidMount() { }


    onChange(name, val) {
        this.setState({ [name]: 3 });
    }
    render() {
        let { loader, secure, audio, light, vibrate, setIsEnabled } = this.state;

        return (
            <ImageBackground
                style={{ flex: 1 }}
                resizeMode="cover"
                // resizeMode="contain"
                source={require('../../assets/images/bg1.png')}>
                <ScrollView>
                    <View style={LoginStyle.top1}>
                        <TouchableOpacity style={{
                            position: "absolute",
                            left: 10, top: 5
                        }} onPress={() => this.props.navigation.goBack()}>

                            <Image
                                style={LoginStyle.back}
                                source={require('../../assets/images/back.png')}
                            />
                        </TouchableOpacity>

                        <View style={{ alignSelf: "center" }}>

                            <Text style={[LoginStyle.color, {
                                fontSize: 18, fontWeight: "bold",

                            }]}>My Bus Stop</Text>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            position: "absolute",
                            right: 10,

                            alignSelf: "center"
                        }}>
                            <TouchableOpacity onPress={() => this.setState({ showNotification: true })} >
                                {false ?
                                    <Image
                                        style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                        source={require('../../assets/images/notification.png')}
                                    /> :
                                    <Image
                                        style={{ width: 25, height: 25, resizeMode: "contain", marginHorizontal: 20 }}
                                        source={require('../../assets/images/rednoti.png')}
                                    />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/images/menu.png')}
                                />
                            </TouchableOpacity>

                        </View>

                    </View>
                    <View>
                        <View style={[LoginStyle.go, { backgroundColor: "#fff" }]}>
                            <Text >Notification</Text>

                        </View>
                        <View style={LoginStyle.notifabg}>
                            <View style={LoginStyle.notifa}>
                                <Text>Audio</Text>
                                <Switch
                                    trackColor={{ false: "#767577", true: "#b2b2b2" }}
                                    thumbColor={audio ? "green" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={(e) => {
                                        this.setState({ audio: e })
                                    }}
                                    value={audio}
                                />
                            </View>
                            <View style={LoginStyle.notifa}>
                                <Text>Vibrate</Text>
                                <Switch
                                    trackColor={{ false: "#767577", true: "#b2b2b2" }}
                                    thumbColor={vibrate ? "green" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={(e) => {
                                        this.setState({ vibrate: e })
                                    }}
                                    value={vibrate}
                                />

                            </View>
                            <View style={[LoginStyle.notifa, { borderBottomColor: "#fff", }]}>
                                <Text>Light Blinking</Text>
                                <Switch
                                    trackColor={{ false: "#767577", true: "#b2b2b2" }}
                                    thumbColor={light ? "green" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={(e) => {
                                        this.setState({ light: e })
                                    }}
                                    value={light}
                                />
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(RoutesKey.CHANGE_PASSWORD)}>

                            <View style={LoginStyle.go}>
                                <Text style={LoginStyle.color}>Change Password</Text>
                                <Image
                                    style={{ width: 20, height: 20, resizeMode: "contain" }}
                                    source={require('../../assets/images/Wright.png')}
                                />
                            </View>
                        </TouchableOpacity>

                    </View>







                </ScrollView>
                {this.state.showModel && (
                    <Alert
                        close={() => {
                            this.setState({ showModel: false });

                        }}
                        buttonHeading={"OK"}
                        msg={"SignUp Functionality in Working"}
                    />
                )}
                {this.state.showNotification && (
                    <Notification
                        close={() => {
                            this.setState({ showNotification: false });

                        }}
                    // buttonHeading={"OK"}
                    // msg={"SignUp Functionality in Working"}
                    />
                )}
            </ImageBackground >
        );
    }
}



