// import React, { Component } from 'react';
import * as React from 'react';

// import { NavigationContainer ,  useFocusEffect,} from '@react-navigation/native';
import { NavigationNativeContainer } from '@react-navigation/native';

import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Routeskey from './routeskey';
import DrawerContent from "./DrawerContent"
import Global from "../utils/global"
import Constant from "../utils/constant"


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

//components list
import Loginsignup from '../components/Login/LoginSignUp'
import Splash from '../screens/splash/Splash';
import Login from "../components/Login/Login"
import Signup from "../components/Login/Signup"
import Forgetpassword from "../components/Login/Forgetpassword"
// Drawer 
import Dashboard from "../components/admin/Dashboard"
import Userdetail from "../components/admin/UserDetail"
import Accounts from "../components/admin/Accounts"
import Advertisers from "../components/admin/Advertiser"
import Advertiserdetail from "../components/admin/AdvertiserDetail"
import Help from "../components/admin/Help"
import Report from "../components/admin/Report"
import Termsandcondition from "../components/advertiser/TermsAndCondition"
import UploadYourAdd from "../components/advertiser/UploadYourAdd"
import Myadd from "../components/advertiser/My_ads"
import Change_password from "../components/advertiser/ChnagePassword"

//User
import Rate from "../components/Users/Rate"
import Setting from "../components/Users/Setting"
import Mybusstop from "../components/Users/MyBusStop"
import Addbusstop from "../components/Users/AddBusStop"
import Profile from "../components/Users/Profile"
import EditProfile from "../components/Users/EditProfile"









export default class routes extends React.Component {
  state = {
    admin: false,
    driver: false,
    advertiser: false
  }
  componentDidMount() {
    this.check()




  }
  check() {
    Global.getDataFromPhone(Constant.USER_DATA_KEY).then(res => {
      if (res) {
        if (res.Role == "driver") {
          this.setState({ driver: true })
        }
        else if (res.Role = "admin") {
          this.setState({ admin: true })
        }
        else if (res.Role = "advertiser") {
          this.setState({ advertiser: true })
        }
      }
      else {
        this.setState({ admin: false })
      }
      // console.log("response of insde phone", res)

    }
    )

  }




  MyDrawer() {
    return (
      <Drawer.Navigator initialRouteName={Routeskey.PROFILE} drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen
          name={Routeskey.DASHBOARD}
          component={Dashboard}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.USER_DETAIL}
          component={Userdetail}
          options={{ drawerLabel: 'Home' }}
        />
        <Drawer.Screen
          name={Routeskey.ACCOUNT}
          component={Accounts}
          options={{ drawerLabel: 'Home' }}
        />
        <Drawer.Screen
          name={Routeskey.ADVERTISER}
          component={Advertisers}
          options={{ drawerLabel: 'Home' }}
        />
        <Drawer.Screen
          name={Routeskey.ADVERTISER_DETAIL}
          component={Advertiserdetail}
          options={{ drawerLabel: 'Home' }}
        />
        <Drawer.Screen
          name={Routeskey.HELP}
          component={Help}
          options={{ drawerLabel: 'Home' }}
        />
        <Drawer.Screen
          name={Routeskey.REPORT}
          component={Report}
          options={{ drawerLabel: 'Home' }}
        />

        <Drawer.Screen
          name={Routeskey.SPLASH}
          component={Splash}
          options={{ drawerLabel: 'Home' }}
        />

        <Drawer.Screen
          name={Routeskey.SETTING}
          component={Setting}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.RATE}
          component={Rate}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.USER_MY_BUS_STOP}
          component={Mybusstop}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.USER_ADD_BUS_STOP}
          component={Addbusstop}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.TERMS_AND_CONDITION}
          component={Termsandcondition}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.PROFILE}
          component={Profile}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.EDIT_PROFILE}
          component={EditProfile}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.CHANGE_PASSWORD}
          component={Change_password}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.UPLOAD_YOUR_ADD}
          component={UploadYourAdd}
          options={{ drawerLabel: 'Hosme' }}
        />
        <Drawer.Screen
          name={Routeskey.MY_ADD}
          component={Myadd}
          options={{ drawerLabel: 'Hosme' }}
        />

      </Drawer.Navigator>

    );
  }
  MyStack() {
    return (
      <Stack.Navigator
        initialRouteName={Routeskey.SPLASH}
        screenOptions={{
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          // ...TransitionPresets.SlideFromRightIOS, // this work the same as cardStyleInterpolator
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          //   TransitionSpecs: {
          //     open: config,
          //     close: closeConfig,
          //   },
        }}>
        <Stack.Screen
          name={Routeskey.SPLASH}
          component={Splash}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={Routeskey.LOFIN_SIGN_UP}
          component={Loginsignup}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name={Routeskey.LOGIN}
          component={Login}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name={Routeskey.SIGN_UP}
          component={Signup}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name={Routeskey.FORGET_PASSWORD}
          component={Forgetpassword}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    )
  }

  getActiveRouteName(state) {
    const route = state.routes[state.index];

    if (route.state) {
      // Dive into nested navigators
      return getActiveRouteName(route.state);
    }

    return route.name;
  };

  render() {
    // console.log("NAVIGSATION",navigationRef)

    const { admin, advertiser, driver } = this.state
    // Global.getDataFromPhone(Constant.USER_DATA_KEY).then(res => {
    //   if (res) {
    //     if (res.Role == "driver") {
    //       return <>{this.MyDrawer()}</>
    //     }
    //     else if (res.Role = "admin") {
    //       return <>{this.MyDrawer()}</>
    //     }
    //     else if (res.Role = "advertiser") {
    //       return <>{this.MyDrawer()}</>
    //     }
    //   }
    //   else {
    //     return <>{this.MyStack()}</>
    //   }
    //   // console.log("response of insde phone", res)
    // }
    // )

    const res = Global.getDataFromPhone(Constant.USER_DATA_KEY)


    // const routeNameRef = React.useRef();
    // const navigationRef = React.useRef();



    // React.useEffect(() => {
    //   const state = navigationRef.current.getRootState();

    //   // Save the initial route name
    //   routeNameRef.current = getActiveRouteName(state);
    // }, []);


    return (




      <>
        <NavigationNativeContainer
          // ref={navigationRef}
          // onStateChange={state => {
          //   const previousRouteName = routeNameRef.current;
          //   const currentRouteName = this.getActiveRouteName(state);

          //   if (previousRouteName !== currentRouteName) {
          //     // The line below uses the @react-native-firebase/analytics tracker
          //     // Change this line to use another Mobile analytics SDK
          //     // analytics().setCurrentScreen(currentRouteName, currentRouteName);
          //     alert(`The route changed to ${currentRouteName}`);
          //   }

          //   // Save the current route name for later comparision
          //   routeNameRef.current = currentRouteName;
          // }}
          onStateChange={(state => {
            // const previousRouteName = routeNameRef.current;
            // const currentRouteName = getActiveRouteName(state);
            if (state.routeNames) {
              if (state.routeNames[state.index] == Routeskey.SPLASH) {
                this.check()

                console.log("ADMIN DATA", admin)
                console.log("routes state", state.routeNames[state.index])
              }

            }
          })
          }
        >

          {admin ? this.MyDrawer() : this.MyStack()}
          {/* {res ? this.MyDrawer() : this.MyStack()} */}


          {/* {this.MyStack()} */}
        </NavigationNativeContainer>

      </>

    );
  }
}
