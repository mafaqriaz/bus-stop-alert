class RoutesKey {
  static SPLASH = 'splash';
  static LOGIN = "login"
  static SIGN_UP = "sign_up"
  static FORGET_PASSWORD = "forget_password"
  static LOFIN_SIGN_UP = "login_sing_up"
  static DASHBOARD = "dashboard"
  static USER_DETAIL = "user_detail"
  static ACCOUNT = "account"
  static ADVERTISER = "advertiser"
  static ADVERTISER_DETAIL = "advertiser_detail"
  static HELP = "help"
  static REPORT = "report"
  static TERMS_AND_CONDITION = "terms_and_condition"
  static RATE = "rate"
  static SETTING = "setting"
  static CHANGE_PASSWORD = "CHange_passwrod"
  static UPLOAD_YOUR_ADD ="upload_your_add"
  static MY_ADD="my_add"

  static PROFILE = "profile"
  static EDIT_PROFILE = "edit_profile"
  static USER_ADD_BUS_STOP = "user_add_busstoip"
  static USER_MY_BUS_STOP = "user_my_bustop"



}

export default RoutesKey;
