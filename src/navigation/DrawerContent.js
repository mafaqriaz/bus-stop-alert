import React, { Component } from 'react';

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { View, Image, StyleSheet, Text, TouchableOpacity } from "react-native"
import RoutesKey from './routeskey';
import Global from "../utils/global"
import Constant from "../utils/constant"
import Advertiser from '../components/admin/Advertiser';
const LogOut = (props) => {
  Global.removeStorage(Constant.USER_DATA_KEY)
  console.log("PROPS", props)

  props.navigation.navigate(RoutesKey.SPLASH)
}
// const navigation = useNavigation();
// import { TouchableOpacity } from 'react-native-gesture-handler';



class CustomDrawerContent extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    const loginData = [
      {
        name: "Dashboard",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.DASHBOARD
      },
      {
        name: "Driver Accounts",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.ACCOUNT
      },
      {
        name: "Get Reports",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.REPORT
      },
      {
        name: "Help",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.HELP
      },
      {
        name: "Advertisers",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.ADVERTISER
      }]


    const Advertisers = [
      {
        name: "Profile",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.PROFILE
      },
      {
        name: "Ads and Areas",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.MY_ADD
      },
      {
        name: "Settings",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.SETTING
      },
      {
        name: "Rate App",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.RATE
      },
      {
        name: "Terms And Condition",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.ADVERTISER
      },
    ]


    const Driver = [
      {
        name: "Profile",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.PROFILE
      },
      {
        name: "My Bus Stops",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.USER_MY_BUS_STOP
      },
      {
        name: "Settings",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.SETTING
      },
      {
        name: "Rate App",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.RATE
      },
      {
        name: "Terms And Condition",
        icon: require("../assets/images/profile_icon.png"),
        key: RoutesKey.TERMS_AND_CONDITION
      },
    ]



    let data = []
    Global.getDataFromPhone(Constant.USER_DATA_KEY).then(res => {
      console.log("response of insde phone", res.Roll)
      if (res.Roll == "admin") {
        data = loginData
        this.props.navigation.navigate(RoutesKey.DASHBOARD)
        this.setState({ data })
      } else if (res.Roll == "advertiser") {
        data = Advertisers
        this.setState({ data })

      } else {
        data = Driver
        this.setState({ data })

      }
    })

  }

  render() {
    const { data } = this.state

    return (
      <DrawerContentScrollView {...this.props}>
        <View style={{ flex: 1, padding: 20 }}>
          {/* <DrawerItemList {...props} /> */}
          <View style={styles.MainContainer}>
            <View style={styles.RoundImage}>
              <Image source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png' }}
                style={{ width: 70, height: 70, borderRadius: 150 / 2 }} />
            </View>
            <View style={styles.NameAndRole}>
              <Text style={styles.Namee}>John Doe</Text>
              <Text style={styles.Rolee}>Driver</Text>
            </View>
          </View>

          <View style={{ flex: 1 }}>

            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate(RoutesKey.USER_DETAIL, { namwe: "check" })} style={styles.Link} >
            <Image source={require("../assets/images/profile_icon.png")}
              style={{ width: 20, height: 20, }} />
            <Text style={styles.LinkName}>{"Profile"}</Text>
          </TouchableOpacity> */}
            {!!data.length && data.map(v =>
              <TouchableOpacity style={styles.Link} onPress={() => this.props.navigation.navigate(v.key)}>
                <Image source={v.icon}
                  style={{ width: 20, height: 20, }} />

                <Text style={styles.LinkName}>{v.name}</Text>
              </TouchableOpacity>)}
            <TouchableOpacity onPress={() => LogOut(this.props)} style={styles.Link} >
              <Image source={require("../assets/images/profile_icon.png")}
                style={{ width: 20, height: 20, }} />

              <Text style={styles.LinkName}>{"Logout"}</Text>
            </TouchableOpacity>
          </View>
          {/* <DrawerItem
          label="Close drawer"
          onPress={() => this.props.navigation.dispatch(DrawerActions.closeDrawer())}
        />
        <DrawerItem
          label="Toggle drawer"
          onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}
        /> */}
        </View>


      </DrawerContentScrollView >
    );
  }
}
export default CustomDrawerContent

const styles = StyleSheet.create(
  {
    MainContainer:
    {
      // backgroundColor: "yellow",
      flex: 1,
      flexDirection: "row",
      justifyContent: 'center',
      alignItems: 'center',

      // margin: 5,
      paddingBottom: 20,
      marginTop: 30,
      paddingTop: (Platform.OS === 'ios') ? 20 : 0,
      borderBottomWidth: 1 / 2,
      borderBottomColor: "gray"

    },
    RoundImage: {
      // backgroundColor: "pink"
    },
    NameAndRole: {
      // backgroundColor: "blue",
      flex: 1,
      paddingHorizontal: 6

    },
    Namee: {
      fontWeight: "bold",
      // fontSize:30,
      // backgroundColor:"pink",
      color: "black"
    },
    Rolee: {
      fontWeight: "100",
      fontSize: 10,
      color: "gray"
    },
    Link: {
      // flex:1,
      flexDirection: "row",
      // backgroundColor: "yellow",
      marginTop: 20,
      paddingVertical: 10
    },
    LinkName: {
      paddingHorizontal: 10,
      fontSize: 12,
      textAlignVertical: "center"


    }

  });