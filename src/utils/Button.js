import React, { Component } from 'react';

import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
  Platform,
  StyleSheet,
} from 'react-native';

class Button extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() { }

  render() {
    return (
      <TouchableOpacity
        // activeOpacity={1}
        style={this.props.bg ? styles.ButtonStyle : styles.ButtonStyle1}
        onPress={this.props.function}>
        <Text style={{ color: '#fff' }}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  button: {
    flexDirection: 'column',
    flex: 10,
    paddingBottom: 10,
  },

  ButtonStyle: {
    backgroundColor: '#46639d',
    width: '90%',
    alignItems: 'center',
    color: 'blue',
    borderRadius: 5,
    alignSelf: 'center',
    marginVertical: 10,
    justifyContent: 'center',
    paddingVertical: 15,
  },
  ButtonStyle1: {
    // backgroundColor: '#07adb9',
    width: '90%',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#46639d',
    color: 'blue',
    borderRadius: 5,
    alignSelf: 'center',
    marginVertical: 10,
    justifyContent: 'center',
    paddingVertical: 15,
  },
});

export default Button;
