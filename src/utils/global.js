import AsyncStorage from '@react-native-community/async-storage';

const Global = {
  saveDataInPhone: async function(datakey, data) {
    try {
      await AsyncStorage.setItem(datakey, JSON.stringify(data));
    } catch (e) {
      console.log(e.message);
    }
  },

  removeStorage: async function(datakey) {
    try {
      await AsyncStorage.removeItem(datakey);
    } catch (e) {
      console.log(e.message);
    }
  },

  getDataFromPhone: async function(datakey) {
    try {
      const value = await AsyncStorage.getItem(datakey);
      if (value !== null) {
        return value;
      }
    } catch (e) {
      console.log(e.message);
    }
  },
};

export default Global;
