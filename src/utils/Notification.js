import React, { Component } from "react";
import { View, Text, Modal, TouchableOpacity, Image, ScrollView } from "react-native";


export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bgw: true,
            Message: true
        };
    }

    bgwhite = () => {
        return (
            <TouchableOpacity
                onPress={this.props.close}
                style={{
                    backgroundColor: 'rgba(52, 52, 52, 0.8)',
                    opacity: 0.9,
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    justifyContent: "center",
                    alignItems: "center",

                }}
            ></TouchableOpacity>
        );
    };

    Message = () => {
        return (

            <View
                style={{
                    padding: 20,
                    width: "100%",

                }}
            >
                <View
                    elevation={5}
                    style={{
                        width: "100%",
                        // height:"80%",
                        borderBottomRightRadius: 10,
                        borderBottomLeftRadius: 10,

                    }}
                >
                    <View
                        style={{
                            padding: 10,


                            borderTopRightRadius: 10,
                            borderTopLeftRadius: 10,
                            borderBottomRightRadius: 10,
                            borderBottomLeftRadius: 10,

                            backgroundColor: "white",
                            // justifyContent: "center",
                            // alignItems: "center",
                            // height: "90%"
                        }}
                    >






                        <View style={{
                            width: "98%",
                            flexDirection: "row",
                            justifyContent: "center",
                            borderRadius: 5,
                            marginVertical: 10,
                            paddingVertical: 5,
                            // marginRight: 15,
                        }}>

                            <View style={{ alignSelf: "center" }}>

                                <Text style={{

                                    fontSize: 20, fontWeight: "bold",
                                    color: "black"

                                }}>Notification</Text>
                            </View>
                            <View style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                position: "absolute",
                                right: 10,

                                alignSelf: "center"
                            }}>

                                <TouchableOpacity
                                    onPress={this.props.close}
                                >

                                    <Image
                                        style={{ width: 23, height: 23 }}
                                        source={require('../assets/images/closeb.png')}
                                    />
                                </TouchableOpacity>

                            </View>

                        </View>



                        {[1, 2, 3, 4, 5].map((v, i) => <View style={{
                            flexDirection: "row",
                            //  backgroundColor: "green",
                            borderColor: 4 == i ? "white" : "gray",
                            borderBottomWidth: 1,
                            paddingBottom: 10

                        }}>
                            <Image
                                style={{
                                    alignSelf: 'center',
                                    resizeMode: 'contain',
                                    width: 50,
                                    height: 50,
                                    marginVertical: 2,
                                    marginHorizontal: 10,
                                    borderBottomWidth: 1,
                                }}
                                source={require('../assets/images/busb.png')}
                            />
                            <View style={{ flexDirection: "column" }}>
                                <Text style={{
                                    fontSize: 15,
                                    color: "black"
                                }}>New Bus Stop</Text>
                                <Text style={{
                                    fontSize: 12,
                                    color: "black",
                                    width: "35%"
                                }}>Lorem Ipsum is simply dummy text of the printing. Lorem Ipsum is simply dummy text of the printing.</Text>

                                <View style={{ flexDirection: "row", paddingTop: 8 }}>
                                    <Image
                                        style={{ width: 23, height: 20 }}
                                        source={require('../assets/images/map_icon.png')}
                                    />
                                    <Text style={{
                                        fontSize: 12,
                                        color: "black",
                                        width: "35%",
                                        paddingHorizontal: 10
                                    }}>0.1km</Text>

                                </View>



                            </View>

                        </View>)}




                    </View>


                </View>
            </View>
        );
    };

    render() {
        return (
            <Modal
                style={{

                }}
                visible={this.state.showModel}
            >

                <View
                    style={{
                        position: "absolute",
                        width: "100%",
                        height: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    {/* <ScrollView> */}
                    {this.state.bgw ? this.bgwhite() : null}
                    {this.state.bgw ? this.Message() : null}
                    {/* </ScrollView> */}
                </View>
            </Modal>
        );
    }
}