import * as firebase from "firebase";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyAUjpPPUMOrmTlSzK7qvR1XupnCyjlkyfs",
  authDomain: "bus-stop786.firebaseapp.com",
  databaseURL: "https://bus-stop786.firebaseio.com",
  projectId: "bus-stop786",
  storageBucket: "bus-stop786.appspot.com",
  messagingSenderId: "383603328201",
  appId: "1:383603328201:web:0acfaa88fbc6f2b2d3b918"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase.analytics();


export default firebase;
